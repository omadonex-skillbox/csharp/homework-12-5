﻿using HomeWork_12_5.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HomeWork_12_5
{
    /// <summary>
    /// Interaction logic for WindowSend.xaml
    /// </summary>
    public partial class WindowSend : Window
    {
        private WindowMain parent;
        public WindowSend(WindowMain parent)
        {
            InitializeComponent();
            this.parent = parent;
        }

        private void ButtonSend_Click(object sender, RoutedEventArgs e)
        {
            if ((ComboBoxFrom.SelectedIndex == -1) || (ComboBoxTo.SelectedIndex == -1))
            {
                MessageBox.Show("Choose both accounts from and to");
            } 
            else if ((ComboBoxFrom.SelectedIndex == ComboBoxTo.SelectedIndex))
            {
                MessageBox.Show("From and to accounts cannot be same");
            }
            else if (TextBoxMoney.Text == "")
            {
                MessageBox.Show("Set amount of money");
            }
            else
            {
                double money = double.Parse(TextBoxMoney.Text);
                Account accountFrom = parent.user.GetAccount((int)ComboBoxFrom.SelectedValue);
                if (accountFrom.Money < money)
                {
                    MessageBox.Show("Amount of money for sending is lesser then amount on account");
                }
                else
                {
                    Account accountTo = parent.user.GetAccount((int)ComboBoxTo.SelectedValue);
                    parent.user.ConsumptionFromAccount(accountFrom.Id, money);
                    parent.user.IncomeToAccount(accountTo.Id, money);
                    Close();
                }                
            }
        }

        private void Window_ContentRendered(object sender, EventArgs e)
        {
            ComboBoxFrom.ItemsSource = parent.user.GetAccountList();
            ComboBoxTo.ItemsSource = parent.user.GetAccountList();
        }
    }
}
