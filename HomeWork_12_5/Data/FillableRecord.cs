﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork_12_5.Data
{
    public abstract class FillableRecord : INotifyPropertyChanged
    {
        [JsonProperty("id")]
        protected int id;

        public event PropertyChangedEventHandler PropertyChanged;
        protected void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        [JsonIgnore]
        public int Id { get { return id; } set { } }

        public FillableRecord()
        {
            id = 0;
        }

        public FillableRecord(int id, Dictionary<string, object> fillableData)
        {
            fill(id, fillableData);
        }

        public void fill(int id, Dictionary<string, object> fillableData)
        {
            this.id = id;
            foreach (string field in fillableData.Keys)
            {
                if (fillableData[field] != null)
                {
                    FieldInfo fi = GetType().GetField(field, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
                    fi.SetValue(this, fillableData[field]);
                }
            }
        }

        public void updateData(Dictionary<string, object> newData)
        {
            foreach (KeyValuePair<string, object> pair in newData)
            {
                if (pair.Value != null && pair.Value != "")
                {
                    FieldInfo fi = GetType().GetField(pair.Key, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
                    fi.SetValue(this, pair.Value);
                }
            }
        }
    }
}
