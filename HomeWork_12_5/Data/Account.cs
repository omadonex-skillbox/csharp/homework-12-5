﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork_12_5.Data
{
    public class Account : FillableRecord
    {
        [JsonProperty("number")]
        private string number;
        [JsonProperty("clientId")]
        private int clientId;
        [JsonProperty("money")]
        private double money;
        [JsonProperty("isDeposit")]
        private bool isDeposit;
        [JsonIgnore]
        public string Number { get { return number; } set { number = value; NotifyPropertyChanged(); } }
        [JsonIgnore]
        public int ClientId { get { return clientId; } set { clientId = value; NotifyPropertyChanged(); } }
        [JsonIgnore]
        public double Money { get { return money; } set { money = value; NotifyPropertyChanged(); } }
        [JsonIgnore]
        public bool IsDeposit { get {  return isDeposit; } set {  isDeposit = value; NotifyPropertyChanged(); } }
        [JsonIgnore]
        public string TypeStr { get { return isDeposit ? "Deposit" : "Non-deposit"; } }

        private double testMoney = 0;
        public void makePayment(double money)
        {
            testMoney += money;
        }
        
    }
}
