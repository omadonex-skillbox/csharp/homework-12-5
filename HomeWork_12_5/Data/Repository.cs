﻿using HomeWork_12_5.Data;
using HomeWork_12_5.Users;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork_12_5
{
    public class Repository<T> : INotifyPropertyChanged
        where T : FillableRecord, new()
    {
        private int maxId;
        private string path;
        private ObservableCollection<T> data;

        public Repository(string path)
        {
            data = new ObservableCollection<T>();
            data.CollectionChanged += CollectionChangedEvent;
            maxId = 0;
            this.path = path;
            Load();         
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void CollectionChangedEvent(object sender, NotifyCollectionChangedEventArgs e)
        {
            NotifyPropertyChanged("data");
        }

        private int NextId()
        {
            return ++maxId;
        }

        private void Load()
        {            
            if (File.Exists(path))
            {
                string[] lines = File.ReadAllLines(path);
                foreach (string line in lines)
                {
                    T t = JsonConvert.DeserializeObject<T>(line);
                    if (maxId < t.Id)
                    {
                        maxId = t.Id;
                    }

                    data.Add(t);                    
                }
                
            }
            else {
                Console.WriteLine("Database not exists!");
                File.Create(path).Close();
                Console.WriteLine("New database was created.");                
            }
        }

        public int Count()
        {
            return data.Count;
        }

        public ObservableCollection<T> All()
        {
            return data;
        }        

        public T Add(User user, Dictionary<string, object> fillableData)
        {            
            T t = new T();
            t.fill(NextId(), fillableData);            
            data.Add(t);
            
            string[] lines = new string[1];
            lines[0] = JsonConvert.SerializeObject(t);
            File.AppendAllLines(path, lines);

            return t;
        }

        public void UpdateById(User user, int id, Dictionary<string, object> newData)
        {
            T t = GetById(id);
            t.updateData(newData);            
            saveData();
        }

        public void DeleteById(User user, int id)
        {            
            data.Remove(GetById(id));
            saveData();
        }

        public T GetById(int id)
        {            
            T t = data.Where(item => item.Id == id).First();

            if (t == null)
            {
                throw new Exception("Not found");
            }           

            return t;
        }

        public bool ExistsById(int id)
        {
            return data.Where(item => item.Id == id).Any();            
        }        

        private void saveData()
        {
            string[] lines = new string[data.Count];
            int index = 0;
            foreach (T t in data)
            {
                lines[index] = JsonConvert.SerializeObject(t);
                index++;
            }

            File.WriteAllLines(path, lines);
        }
    }
}
