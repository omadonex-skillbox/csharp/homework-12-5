﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Markup;
using HomeWork_12_5.Data;

namespace HomeWork_12_5.Users
{
    public class User
    {
        protected Repository<Client> repositoryClient;
        protected Repository<Account> repositoryAccount;

        public User(Repository<Client> repositoryClient, Repository<Account> repositoryAccount)
        {
            this.repositoryClient = repositoryClient;
            this.repositoryAccount = repositoryAccount;
        }
       
        public ObservableCollection<Client> GetClientList()
        {
            return repositoryClient.All();
        }

        public int GetClientCount()
        {
            return repositoryClient.Count();
        }

        public ObservableCollection<Account> GetAccountList()
        {
            return repositoryAccount.All();
        }

        public int GetAccountCount()
        {
            return repositoryAccount.Count();
        }

        public ObservableCollection<Account> GetAccountList(int clientId)
        {
            return new ObservableCollection<Account>(repositoryAccount.All().Where(account => account.ClientId == clientId));            
        }

        public void AddClient(Dictionary<string, object> data)
        {
            repositoryClient.Add(this, data);
        }
        public Account AddAccount(int clientId, bool isDeposit, bool randomMoney)
        {
            return repositoryAccount.Add(this, new Dictionary<string, object>()
            {
                { "number", Guid.NewGuid().ToString() },
                { "clientId", clientId },
                { "money", randomMoney ? Math.Round((new Random()).NextDouble() * 100000, 2) : 0 },
                { "isDeposit", isDeposit },
            });
        }

        public void IncomeToAccount(int accountId, double? money = null)
        {
            Account account = repositoryAccount.GetById(accountId);            
            repositoryAccount.UpdateById(this, accountId, new Dictionary<string, object>()
            {
                { "money", account.Money + (money == null ? Math.Round((new Random()).NextDouble() * 1000, 2) : (double)money) },
            });
        }

        public void ConsumptionFromAccount(int accountId, double? money = null)
        {
            Account account = repositoryAccount.GetById(accountId);
            repositoryAccount.UpdateById(this, accountId, new Dictionary<string, object>()
            {
                { "money", account.Money - (money == null ? Math.Round((new Random()).NextDouble() * 1000, 2) : (double)money) },
            });
        }

        public void DeleteAccount(int accountId)
        {
            repositoryAccount.DeleteById(this, accountId);
        }

        public Account GetAccount(int accountId)
        {
            return repositoryAccount.GetById(accountId);
        }
        public Account GetAccount(int clientId, bool isDeposit) 
        {
            return repositoryAccount.All().First(item => item.ClientId == clientId && item.IsDeposit == isDeposit);
        }

        public bool HasClientAccount(int clientId, bool isDeposit)
        {
            return repositoryAccount.All().Where(item => item.ClientId == clientId && item.IsDeposit == isDeposit).Any();
        }
    }
}
